package me.porkchop.ruse;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.GameSettings;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class Wrapper {

	/**
	 * Returns Minecraft
	 */
	public static Minecraft getMinecraft() {
		return Minecraft.getMinecraft();
	}
	
	
	/**
	 * Returns thePlayer
	 */
	public static EntityClientPlayerMP getPlayer() {	
		return Minecraft.getMinecraft().thePlayer;
	}
	
	
	/**
	 * Returns theWorld
	 */
	public static WorldClient getWorld() {
		return Minecraft.getMinecraft().theWorld;
	}
	
	
	/**
	 * Returns GameSettings
	 */
	public static GameSettings getGameSettings() {
		return Minecraft.getMinecraft().gameSettings;
	}
	
	/**
	 * Returns FontRenderer
	 */
	public static FontRenderer getFontRenderer() {
		return Minecraft.getMinecraft().fontRenderer;
	}

}
