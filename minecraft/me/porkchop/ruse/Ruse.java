package me.porkchop.ruse;

import me.porkchop.ruse.module.utils.ModuleCategory;
import me.porkchop.ruse.module.utils.ModuleKeyManager;
import me.porkchop.ruse.module.utils.ModuleManager;
import me.porkchop.ruse.ui.RuseUI;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class Ruse {

	/**
	 * Creates a new instance of Ruse
	 */	
	private static Ruse instance = new Ruse();
	
	
	/**
	 * Creates a new instance of ModuleManager
	 */
	private ModuleManager moduleManager = new ModuleManager();
	
	
	/**
	 * Creates a new instance of ModuleKeyManager
	 */
	private ModuleKeyManager moduleKeyManager = new ModuleKeyManager();
	
	
	/**
	 * Creates a new instance of RuseUI
	 */
	private RuseUI uiManager = new RuseUI();

	
	/**
	 * Creates a String for the command prefix
	 * TODO: Use primitive data type 'char' instead
	 * TODO: Make a setter for 'commandPrefix'
	 */	
	private String commandPrefix = "."; 
	
	
	/**
	 * Returns an instance of Ruse
	 */
	public static Ruse getInstance() {
		return instance;
	}
	
	
	/**
	 * Returns name
	 */
	public String getName() {
		return "Ruse";
	}
	

	/**
	 * Returns version
	 */
	public double getVersion() {
		return 0.1;
	}
	
	/**
	 * Returns the command prefix.
	 */
	public String getPrefix() {
		return commandPrefix;
	}
	
	
	/**
	 * Returns an instance of ModuleManager
	 */
	public ModuleManager getModuleManager() {
		return moduleManager;
	}
	
	
	/**
	 * Returns an instance of ModuleKeyManager
	 */
	public ModuleKeyManager getKeyManager() {
		return moduleKeyManager;
	}
	
	
	/**
	 * Returns an instance of RuseUI
	 */
	public RuseUI getUI() {
		return uiManager;
	}

}
