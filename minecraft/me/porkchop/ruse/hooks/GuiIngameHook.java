package me.porkchop.ruse.hooks;

import me.porkchop.ruse.Ruse;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class GuiIngameHook extends GuiIngame {

	public GuiIngameHook(Minecraft minecraft) {
		super(minecraft);
	}
	
	/**
	 * Overrides the renderGameOverlay method to make it possible to draw our UI
	 */
	@Override
	public void renderGameOverlay(float f, boolean b, int i, int j) {
		super.renderGameOverlay(f, b, i, j);
		Ruse.getInstance().getUI().drawUI();
	}
	
}
