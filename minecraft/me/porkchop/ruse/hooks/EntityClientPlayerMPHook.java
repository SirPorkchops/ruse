package me.porkchop.ruse.hooks;

import me.porkchop.ruse.Ruse;
import me.porkchop.ruse.module.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.util.Session;
import net.minecraft.world.World;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class EntityClientPlayerMPHook extends EntityClientPlayerMP {

	public EntityClientPlayerMPHook(Minecraft p_i45064_1_, World p_i45064_2_,Session p_i45064_3_, NetHandlerPlayClient p_i45064_4_, StatFileWriter p_i45064_5_) {
		super(p_i45064_1_, p_i45064_2_, p_i45064_3_, p_i45064_4_, p_i45064_5_);
	}
	
	/**
	 * Overrides the method of the superclass (EntityClientPlayerMP) to allow our commands to be executed
	 */
	@Override
	public void sendChatMessage(String s) {
		if(s.startsWith(Ruse.getInstance().getPrefix())) {
			for(Module m : Ruse.getInstance().getModuleManager().getModules()) {
				m.handleCommand(s);
			}
		} else {
			/**
			 * Calls the superclass' method
			 */
			super.sendChatMessage(s);
		}
	}
	
	/**
	 * Overrides the sendMotionUpdate method with the onPreMotion and onPostMotion methods from our Module base
	 */
	@Override
	public void sendMotionUpdates() {
		for(Module m : Ruse.getInstance().getModuleManager().getModules()) { m.onPreMotion(); }
		super.sendMotionUpdates();
		for(Module m : Ruse.getInstance().getModuleManager().getModules()) { m.onPostMotion(); }
	}

}
