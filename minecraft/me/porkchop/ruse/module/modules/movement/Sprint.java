package me.porkchop.ruse.module.modules.movement;

/**
 * @author PorkchopHF
 * @since July 28, 2014
 */

import me.porkchop.ruse.Ruse;
import me.porkchop.ruse.module.Module;
import me.porkchop.ruse.module.utils.ModuleCategory;

public class Sprint extends Module {
	

	/**
	 * Sets the constructor with information about the Module Sprint
	 */
	public Sprint() {
		super("Sprint", "Makes you sprint.", "G", ModuleCategory.PLAYER);
	}

	/**
	 * Checks if the player canSprint before setting sprinting to true
	 */
	@Override
	public void onPreMotion() {
		if(canSprint()) 
			getPlayer().setSprinting(true);
	}

	/**
	 * On the disable of the Module, it will set sprinting to false
	 */
	@Override
	public void onDisable() {
		getPlayer().setSprinting(false);
	}
	
	/**
	 * A check to make sure that the player's forward movement is greater than 0F, and that they are not collided horizontally
	 */
	public boolean canSprint() {
		return (getPlayer().moveForward > 0.0F) && !getPlayer().isCollidedHorizontally;
	}

}
