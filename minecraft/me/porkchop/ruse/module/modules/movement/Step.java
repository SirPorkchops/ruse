package me.porkchop.ruse.module.modules.movement;

/**
 * @author PorkchopHF
 * @since July 28, 2014
 */

import me.porkchop.ruse.Ruse;
import me.porkchop.ruse.module.Module;
import me.porkchop.ruse.module.utils.ModuleCategory;

public class Step extends Module {
	
	/**
	 * Creates a new float for stepHeight (1F)
	 */
	public float stepHeight = 1F;

	/**
	 * Sets the constructor with information about the Module Step
	 */
	public Step() {
		super("Step", "Allows the player to step up more blocks.", "U", ModuleCategory.PLAYER);
	}

	/**
	 * Checks if the Module is enabled before setting the stepHeight to the custom one declared above on pre-motion
	 */
	@Override
	public void onPreMotion() {
		if (getState()) {
			getPlayer().stepHeight = stepHeight;
		}
	}

	/**
	 * On the disable of the Module, it will set the stepHeight back to default (0.5F). There is no need to use a variable as nothing else in the game modifies the step height
	 */
	@Override
	public void onDisable() {
		getPlayer().stepHeight = 0.5F;
	}

	/**
	 * Handles the command
	 */
	@Override
	public void handleCommand(String cmd) {
		if (cmd.startsWith(Ruse.getInstance().getPrefix() + "sh")) {
			try {
				String[] s1 = cmd.split(" ");
				if(Float.parseFloat(s1[1]) >= 0.5 && Float.parseFloat(s1[1]) <= 256) {
					stepHeight = Float.parseFloat(s1[1]);
				} else {
					System.out.println("Stepheight was either smaller than 0.5, or bigger than 256.");
				}
				System.out.println("Stepheight set to " + stepHeight);
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
	}

}
