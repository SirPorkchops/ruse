package me.porkchop.ruse.module;

import me.porkchop.ruse.Ruse;
import me.porkchop.ruse.Wrapper;
import me.porkchop.ruse.module.utils.ModuleCategory;

import org.lwjgl.input.Keyboard;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class Module extends Wrapper {
	
	/**
	 * Creates a new String for name
	 */
	private String name;
	
	/**
	 * Creates a new String for description
	 */
	private String desc;
	
	/**
	 * Creates a new int for the key
	 */
	private int key;
	
	/**
	 * Creates an instance of ModuleCategory
	 */
	private ModuleCategory category;
	
	/**
	 * Creates a boolean for the state of the Module
	 */
	private boolean state;
	
	/**
	 * Creates a boolean for the visibility of the Module in the UI
	 */
	private boolean visible = true;
	
	/**
	 * 
	 * @param name
	 * @param desc
	 * @param key
	 * @param category
	 * @param visible
	 * @param state
	 */
	public Module(String name, String desc, String key, ModuleCategory category, boolean visible, boolean state) {
		this.name = name;
		this.desc = desc;
		this.key = Keyboard.getKeyIndex(key);
		this.category = category;
		this.visible = visible;
		this.state = state;
	}

	/**
	 * 
	 * @param name
	 * @param desc
	 * @param key
	 * @param category
	 */
	public Module(String name, String desc, String key, ModuleCategory category) {
		this.name = name;
		this.desc = desc;
		this.key = Keyboard.getKeyIndex(key);
		this.category = category;
		this.visible = true;
	}

	/**
	 * Creates various getters and setters for the fields declared above.
	 */
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	public String getDesc() { return desc; }
	public void setDesc(String desc) { this.desc = desc; }
	
	public int getKey() { return key; }
	public void setKey(int key) { this.key = key; }
	
	public ModuleCategory getCategory() { return category; }
	public void setCategory(ModuleCategory category) { this.category = category; }
	
	public boolean getVisible() { return visible; }
	public void setVisible(boolean visible) { this.visible = visible; }
	
	/**
	 * Creates methods to assist in the creation of Modules.
	 */
	public void onEnable() {}
	public void onDisable() {}
	public void onToggle() {}
	public void onRender() {}
	public void onTick() {}
	public void renderOnGui() {}
	public void onPreMotion() {}
	public void onPostMotion() {}
	public void handleCommand(String cmd) {}
	
	/**
	 *  Returns the Module's state.
	 */
	public boolean getState() { return state; }
	
	/**
	 * Sets the module's state.
	 */
	public void setState() { state = !state; onStateChange(); }
	
	/**
	 * Called on the state change of the Module.
	 */
	public void onStateChange() {
		if(state) {
			onEnable(); 
			onToggle(); 
		} else {
		   onDisable();
		   onToggle();
		}
	}
	
}
