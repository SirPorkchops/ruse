package me.porkchop.ruse.module.utils;

import me.porkchop.ruse.Ruse;
import me.porkchop.ruse.module.Module;

import org.lwjgl.input.Keyboard;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class ModuleKeyManager {
	
	/**
	 * Called when the event key is equal to the key of the Module
	 */
	public void onKeyPressed() {
		for(Module m : Ruse.getInstance().getModuleManager().getModules()) {
			if(Keyboard.getEventKey() == m.getKey()) {
				m.setState();
				System.out.println("'" + m.getName() + "' set to " + m.getState() + " [" + Keyboard.getKeyName(m.getKey()) + "]");
			}
		}
	}
	
}
