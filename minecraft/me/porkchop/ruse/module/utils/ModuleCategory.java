package me.porkchop.ruse.module.utils;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public enum ModuleCategory {

	PLAYER(0xFFFFFF),
	COMBAT(0xFFFFFF),
	WORLD(0xFFFFFF),
	RENDER(0xFFFFFF),
	MOVEMENT(0xFFFFFF),
	MISC(0xFFFFFF);
	
	/**
	 * Sets the color in the constructor
	 */
	int color;
	ModuleCategory(int color) {
		this.color = color;
	}
	
	/**
	 * Returns the color of the category
	 */
	public int getColor() {
		return color;
	}

}
