package me.porkchop.ruse.module.utils;

import me.porkchop.ruse.module.Module;
import me.porkchop.ruse.module.modules.combat.*;
import me.porkchop.ruse.module.modules.misc.*;
import me.porkchop.ruse.module.modules.movement.*;
import me.porkchop.ruse.module.modules.player.*;
import me.porkchop.ruse.module.modules.render.*;
import me.porkchop.ruse.module.modules.world.*;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class ModuleManager {

	
	/**
	 * Makes an array of the Modules.
	 * When adding new Modules, add them to this array
	 */
	private Module[] modules = {
		new Step(),	
		new Sprint(),
	};
	
	/**
	 * Returns the array of Modules.
	 */
	public Module [] getModules() {
		return modules;
	}
	
	/**
	 * Returns the Module based on the input.
	 */
	public Module getModuleByName(String s) {
		for(Module m : getModules()) {
			if(s.equalsIgnoreCase(m.getName())) {
				return m;
			}
		}
		return null;
 	}
	
}
