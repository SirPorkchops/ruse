package me.porkchop.ruse.ui;

import me.porkchop.ruse.Ruse;
import me.porkchop.ruse.Wrapper;
import me.porkchop.ruse.module.Module;
import me.porkchop.ruse.module.utils.ModuleCategory;
import net.minecraft.client.gui.ScaledResolution;

	/**
	 * @author PorkchopHF
	 * @since July 28, 2014
	 */

public class RuseUI {
	
	/**
	 * Draws the In Game UI
	 */
	public void drawUI() {
		Wrapper.getFontRenderer().drawStringWithShadow(Ruse.getInstance().getName() + " v" + Ruse.getInstance().getVersion(), 2, 2, 0xFFFFFF);
		renderArrayList();
	}
	
	/**
	 * Renders the right-aligned array list
	 */
	public void renderArrayList() {	
		int y = 2;
		ScaledResolution sr = new ScaledResolution(Wrapper.getGameSettings(), Wrapper.getMinecraft().displayWidth, Wrapper.getMinecraft().displayHeight);
		for(Module m : Ruse.getInstance().getModuleManager().getModules()) {
			if(m.getState() && m.getVisible()) {
				Wrapper.getFontRenderer().drawStringWithShadow(m.getName(), (sr.getScaledWidth() - Wrapper.getFontRenderer().getStringWidth(m.getName())) - 2, y, m.getCategory().getColor());
				y += 10;
			}
		}
	}
	
}
